# This include file sets up the ml config fragments, these
# fragments build on top of the base config infrastructure provided in
# the linux-yocto/kernel-yocto recipes and classes in core layer of OE.

FILESEXTRAPATHS_prepend := "${THISDIR}/config:"

SRC_URI_append += " \
                file://arm64-ml-base;type=kmeta;destsuffix=arm64-ml-base \
                "
# this is not available in meta-virtualization:
FILESEXTRAPATHS_prepend := "${THISDIR}/config/arm64-ml-base:"
