# U-Boot verssion and machine specific changes:

FILESEXTRAPATHS_prepend := "${THISDIR}/${PV}/${MACHINE}:"

SRC_URI_append_raspberrypi4-64-ml = " \
        file://1-2-bcmgenet-fix-DMA-buffer-management.patch \
        file://2-2-bcmgenet-Add-support-for-rgmii-rxid.patch \
"
