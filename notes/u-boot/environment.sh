When I stop U-Boot:

U-Boot> printe
arch=arm
baudrate=115200
board=rpi
board_name=4 Model B
board_rev=0x11
board_rev_scheme=1
board_revision=0xD03114
boot_a_script=load ${devtype} ${devnum}:${distro_bootpart} ${scriptaddr} ${prefix}${script}; source ${scriptaddr}
boot_efi_binary=if fdt addr ${fdt_addr_r}; then bootefi bootmgr ${fdt_addr_r};else bootefi bootmgr ${fdtcontroladdr};fi;load ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r} efi/boot/bootaa64.efi; if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r};else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi
boot_extlinux=sysboot ${devtype} ${devnum}:${distro_bootpart} any ${scriptaddr} ${prefix}${boot_syslinux_conf}
boot_prefixes=/ /boot/
boot_script_dhcp=boot.scr.uimg
boot_scripts=boot.scr.uimg boot.scr
boot_syslinux_conf=extlinux/extlinux.conf
boot_targets=mmc0 mmc1 pxe dhcp
bootargs=coherent_pool=1M 8250.nr_uarts=1 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1 snd_bcm2835.enable_headphones=1  smsc95xx.macaddr=DC:A6:32:B8:81:F8 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  dwc_otg.lpm_enable=0 console=ttyS0,115200 root=/dev/nfs rootfstype=nfs nfsroot=192.168.42.108:/opt/poky/raspi-4-rootfs,v3,tcp ip=dhcp rootwait
bootcmd=run distro_bootcmd
bootcmd_dhcp=if dhcp ${scriptaddr} ${boot_script_dhcp}; then source ${scriptaddr}; fi;setenv efi_fdtfile ${fdtfile}; setenv efi_old_vci ${bootp_vci};setenv efi_old_arch ${bootp_arch};setenv bootp_vci PXEClient:Arch:00011:UNDI:003000;setenv bootp_arch 0xb;if dhcp ${kernel_addr_r}; then tftpboot ${fdt_addr_r} dtb/${efi_fdtfile};if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r}; else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi;fi;setenv bootp_vci ${efi_old_vci};setenv bootp_arch ${efi_old_arch};setenv efi_fdtfile;setenv efi_old_arch;setenv efi_old_vci;
bootcmd_mmc0=devnum=0; run mmc_boot
bootcmd_mmc1=devnum=1; run mmc_boot
bootcmd_pxe=dhcp; if pxe get; then pxe boot; fi
bootdelay=2
bootfstype=fat
cpu=armv8
devplist=1
dhcpuboot=usb start; dhcp u-boot.uimg; bootm
distro_bootcmd=for target in ${boot_targets}; do run bootcmd_${target}; done
efi_dtb_prefixes=/ /dtb/ /dtb/current/
ethaddr=dc:a6:32:b8:81:f8
fdt_addr=2eff4100
fdt_addr_r=0x02600000
fdt_high=ffffffffffffffff
fdtaddr=2eff4100
fdtcontroladdr=3af57f10
fdtfile=broadcom/bcm2711-rpi-4-b.dtb
fileaddr=80000
filesize=1cc2a00
initrd_high=ffffffffffffffff
kernel_addr_r=0x00080000
load_efi_dtb=load ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r} ${prefix}${efi_fdtfile}
loadaddr=0x00200000
mmc_boot=if mmc dev ${devnum}; then devtype=mmc; run scan_dev_for_boot_part; fi
pxefile_addr_r=0x02500000
ramdisk_addr_r=0x02700000
scan_dev_for_boot=echo Scanning ${devtype} ${devnum}:${distro_bootpart}...; for prefix in ${boot_prefixes}; do run scan_dev_for_extlinux; run scan_dev_for_scripts; done;run scan_dev_for_efi;
scan_dev_for_boot_part=part list ${devtype} ${devnum} -bootable devplist; env exists devplist || setenv devplist 1; for distro_bootpart in ${devplist}; do if fstype ${devtype} ${devnum}:${distro_bootpart} bootfstype; then run scan_dev_for_boot; fi; done; setenv devplist
scan_dev_for_efi=setenv efi_fdtfile ${fdtfile}; for prefix in ${efi_dtb_prefixes}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${efi_fdtfile}; then run load_efi_dtb; fi;done;if test -e ${devtype} ${devnum}:${distro_bootpart} efi/boot/bootaa64.efi; then echo Found EFI removable media binary efi/boot/bootaa64.efi; run boot_efi_binary; echo EFI LOAD FAILED: continuing...; fi; setenv efi_fdtfile
scan_dev_for_extlinux=if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${boot_syslinux_conf}; then echo Found ${prefix}${boot_syslinux_conf}; run boot_extlinux; echo SCRIPT FAILED: continuing...; fi
scan_dev_for_scripts=for script in ${boot_scripts}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${script}; then echo Found U-Boot script ${prefix}${script}; run boot_a_script; echo SCRIPT FAILED: continuing...; fi; done
scriptaddr=0x02400000
serial#=1000000082526380
soc=bcm283x
stderr=serial,vidconsole
stdin=serial,usbkbd
stdout=serial,vidconsole
usbethaddr=dc:a6:32:b8:81:f8
vendor=raspberrypi

Environment size: 4140/16380 bytes
U-Boot> 

---------------------------

manually kernel/fdt from SD card - rootfs over nfs

U-Boot> ls mmc 0:1
    47463   bcm2711-rpi-4-b.dtb
    52480   bootcode.bin
        0   bootfiles-20200713.stamp
      262   boot.scr
      149   cmdline.txt
    36330   config.txt
     3148   fixup4cd.dat
     5409   fixup4.dat
     8414   fixup4db.dat
     8418   fixup4x.dat
     3148   fixup_cd.dat
     7274   fixup.dat
    10266   fixup_db.dat
    10264   fixup_x.dat
 30157312   Image
   442800   kernel8.img
            overlays/
   818908   start4cd.elf
  3778856   start4db.elf
  2277248   start4.elf
  3036104   start4x.elf
   818908   start_cd.elf
  4850760   start_db.elf
  3001472   start.elf
  3759624   start_x.elf
    16384   uboot.env

25 file(s), 1 dir(s)

U-Boot> echo ${scriptaddr}
0x02400000

devtype is most likely mmc

load ${devtype} ${devnum}:${distro_bootpart} ${scriptaddr} ${prefix}${script}; source ${scriptaddr}

load mmc 0:1 ${scriptaddr} boot.scr

source ${scriptaddr}

but now it starts

hmm something in there starts it up

let's see from Linux what boot.scr does

root@raspberrypi4-64:~# mount /dev/mmcblk1
mmcblk1    mmcblk1p1  mmcblk1p2
root@raspberrypi4-64:~# mount /dev/mmcblk1p1 /mnt/mala
root@raspberrypi4-64:~# cd /mnt/mala/
root@raspberrypi4-64:/mnt/mala# ls
Image                     cmdline.txt               fixup4db.dat              kernel8.img               start4db.elf              uboot.env
bcm2711-rpi-4-b.dtb       config.txt                fixup4x.dat               overlays                  start4x.elf
boot.scr                  fixup.dat                 fixup_cd.dat              start.elf                 start_cd.elf
bootcode.bin              fixup4.dat                fixup_db.dat              start4.elf                start_db.elf
bootfiles-20200713.stamp  fixup4cd.dat              fixup_x.dat               start4cd.elf              start_x.elf
root@raspberrypi4-64:/mnt/mala# 
root@raspberrypi4-64:/mnt/mala# cat boot.scr 
'V���~�֬�Boot script�fdt addr ${fdt_addr} && fdt get value bootargs /chosen bootargs
fatload mmc 0:1 ${kernel_addr_r} Image
if test ! -e mmc 0:1 uboot.env; then saveenv; fi;
booti ${kernel_addr_r} - ${fdt_addr}

root@raspberrypi4-64:/mnt/mala# cat uboot.env
U"arch=armbaudrate=115200board=rpiboard_name=4 Model Bboard_rev=0x11board_rev_scheme=1board_revision=0xD03114boot_a_script=load ${devtype} ${devnum}:${distro_bootpart} ${scriptaddr} ${prefix}${script}; source ${scriptaddr}boot_efi_binary=if fdt addr ${fdt_addr_r}; then bootefi bootmgr ${fdt_addr_r};else bootefi bootmgr ${fdtcontroladdr};fi;load ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r} efi/boot/bootaa64.efi; if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r};else bootefi ${kernel_addr_r} ${fdtcontroladdr};fiboot_extlinux=sysboot ${devtype} ${devnum}:${distro_bootpart} any ${scriptaddr} ${prefix}${boot_syslinux_conf}boot_prefixes=/ /boot/boot_script_dhcp=boot.scr.uimgboot_scripts=boot.scr.uimg boot.scrboot_syslinux_conf=extlinux/extlinux.confboot_targets=mmc0 mmc1 pxe dhcp bootargs=coherent_pool=1M 8250.nr_uarts=1 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1 snd_bcm2835.enable_headphones=1  smsc95xx.macaddr=DC:A6:32:B8:81:F8 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  dwc_otg.lpm_enable=0 console=ttyS0,115200 root=/dev/nfs rootfstype=nfs nfsroot=192.168.42.108:/opt/poky/raspi-4-rootfs,v3,tcp ip=dhcp rootwaitbootcmd=run distro_bootcmdbootcmd_dhcp=if dhcp ${scriptaddr} ${boot_script_dhcp}; then source ${scriptaddr}; fi;setenv efi_fdtfile ${fdtfile}; setenv efi_old_vci ${bootp_vci};setenv efi_old_arch ${bootp_arch};setenv bootp_vci PXEClient:Arch:00011:UNDI:003000;setenv bootp_arch 0xb;if dhcp ${kernel_addr_r}; then tftpboot ${fdt_addr_r} dtb/${efi_fdtfile};if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r}; else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi;fi;setenv bootp_vci ${efi_old_vci};setenv bootp_arch ${efi_old_arch};setenv efi_fdtfile;setenv efi_old_arch;setenv efi_old_vci;bootcmd_mmc0=devnum=0; run mmc_bootbootcmd_mmc1=devnum=1; run mmc_bootbootcmd_pxe=dhcp; if pxe get; then pxe boot; fibootdelay=2bootfstype=fatcpu=armv8devplist=1dhcpuboot=usb start; dhcp u-boot.uimg; bootmdistro_bootcmd=for target in ${boot_targets}; do run bootcmd_${target}; doneefi_dtb_prefixes=/ /dtb/ /dtb/current/ethaddr=dc:a6:32:b8:81:f8fdt_addr=2eff4100fdt_addr_r=0x02600000fdt_high=fffffffffffffffffdtaddr=2eff4100fdtcontroladdr=3af57f10fdtfile=broadcom/bcm2711-rpi-4-b.dtbfileaddr=80000filesize=1cc2a00initrd_high=ffffffffffffffffkernel_addr_r=0x00080000load_efi_dtb=load ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r} ${prefix}${efi_fdtfile}loadaddr=0x00200000mmc_boot=if mmc dev ${devnum}; then devtype=mmc; run scan_dev_for_boot_part; fipxefile_addr_r=0x02500000ramdisk_addr_r=0x02700000scan_dev_for_boot=echo Scanning ${devtype} ${devnum}:${distro_bootpart}...; for prefix in ${boot_prefixes}; do run scan_dev_for_extlinux; run scan_dev_for_scripts; done;run scan_dev_for_efi;scan_dev_for_boot_part=part list ${devtype} ${devnum} -bootable devplist; env exists devplist || setenv devplist 1; for distro_bootpart in ${devplist}; do if fstype ${devtype} ${devnum}:${distro_bootpart} bootfstype; then run scan_dev_for_boot; fi; done; setenv devplistscan_dev_for_efi=setenv efi_fdtfile ${fdtfile}; for prefix in ${efi_dtb_prefixes}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${efi_fdtfile}; then run load_efi_dtb; fi;done;if test -e ${devtype} ${devnum}:${distro_bootpart} efi/boot/bootaa64.efi; then echo Found EFI removable media binary efi/boot/bootaa64.efi; run boot_efi_binary; echo EFI LOAD FAILED: continuing...; fi; setenv efi_fdtfilescan_dev_for_extlinux=if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${boot_syslinux_conf}; then echo Found ${prefix}${boot_syslinux_conf}; run boot_extlinux; echo SCRIPT FAILED: continuing...; fiscan_dev_for_scripts=for script in ${boot_scripts}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${script}; then echo Found U-Boot script ${prefix}${script}; run boot_a_script; echo SCRIPT FAILED: continuing...; fi; donescriptaddr=0x02400000serial#=1000000082526380soc=bcm283xstderr=sroot@raspberrypi4-64:/mnt/mala# vi uboot.env
root@raspberrypi4-64:/mnt/mala# 

Hmm make it better readable:

U"arch=arm                                                                                                                                                                   
^@baudrate=115200^@                                                                                                                                                          
board=rpi^@                                                                                                                                                                  
board_name=4 Model B^@                                                                                                                                                       
board_rev=0x11^@                                                                                                                                                             
board_rev_scheme=1^@                                                                                                                                                         
board_revision=0xD03114^@                                                                                                                                                    
boot_a_script=load ${devtype} ${devnum}:${distro_bootpart} ${scriptaddr} ${prefix}${script}; source ${scriptaddr}^@                                                          
boot_efi_binary=if fdt addr ${fdt_addr_r}; then bootefi bootmgr ${fdt_addr_r};else bootefi bootmgr ${fdtcontroladdr};fi;load ${devtype} ${devnum}:${distro_bootpart} ${kernel
boot_extlinux=sysboot ${devtype} ${devnum}:${distro_bootpart} any ${scriptaddr} ${prefix}${boot_syslinux_conf}^@                                                             
boot_prefixes=/ /boot/^@                                                                                                                                                     
boot_script_dhcp=boot.scr.uimg^@                                                                                                                                             
boot_scripts=boot.scr.uimg boot.scr^@                                                                                                                                        
boot_syslinux_conf=extlinux/extlinux.conf^@                                                                                                                                  
boot_targets=mmc0 mmc1 pxe dhcp ^@                                                                                                                                           
bootargs=coherent_pool=1M 8250.nr_uarts=1 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1 snd_bcm2835.enable_headphones=1                                         
smsc95xx.macaddr=DC:A6:32:B8:81:F8 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  wc_otg.lpm_enable=0 console=ttyS0,115200 root=/dev/nfs rootfstype=nfs nfsroot=192.1
bootcmd=run distro_bootcmd^@                                                                                                                                                 
bootcmd_dhcp=if dhcp ${scriptaddr} ${boot_script_dhcp}; then source ${scriptaddr}; fi;setenv efi_fdtfile ${fdtfile}; setenv efi_old_vci ${bootp_vci};setenv efi_old_arch ${bo
bootcmd_mmc0=devnum=0; run mmc_boot^@                                                                                                                                        
bootcmd_mmc1=devnum=1; run mmc_boot^@                                                                                                                                        
bootcmd_pxe=dhcp; if pxe get; then pxe boot; fi^@                                                                                                                            
bootdelay=2^@                                                                                                                                                                
bootfstype=fat^@                                                                                                                                                             
cpu=armv8^@                                                                                                                                                                  
devplist=1^@                                                                                                                                                                 
dhcpuboot=usb start; dhcp u-boot.uimg; bootm^@                                                                                                                               
distro_bootcmd=for target in ${boot_targets}; do run bootcmd_${target}; done^@                                                                                               
efi_dtb_prefixes=/ /dtb/ /dtb/current/^@                                                                                                                                     
ethaddr=dc:a6:32:b8:81:f8^@                                                                                                                                                  
fdt_addr=2eff4100^@                                                                                                                                                          
fdt_addr_r=0x02600000^@                                                                                                                                                      
fdt_high=ffffffffffffffff^@                                                                                                                                                  
fdtaddr=2eff4100^@                                                                                                                                                           
fdtcontroladdr=3af57f10^@                                                                                                                                                    
fdtfile=broadcom/bcm2711-rpi-4-b.dtb^@                                                                                                                                       
fileaddr=80000^@                                                                                                                                                             
filesize=1cc2a00^@                                                                                                                                                           
initrd_high=ffffffffffffffff^@                                                                                                                                               
kernel_addr_r=0x00080000^@                                                                                                                                                   
load_efi_dtb=load ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r} ${prefix}${efi_fdtfile}^@                                                                            
loadaddr=0x00200000^@                                                                                                                                                        
mmc_boot=if mmc dev ${devnum}; then devtype=mmc; run scan_dev_for_boot_part; fi^@
pxefile_addr_r=0x02500000^@                                                                                                                                                  
ramdisk_addr_r=0x02700000^@                                                                                                                                                  
scan_dev_for_boot=echo Scanning ${devtype} ${devnum}:${distro_bootpart}...; for prefix in ${boot_prefixes}; do run scan_dev_for_extlinux; run scan_dev_for_scripts; done;run 
scan_dev_for_boot_part=part list ${devtype} ${devnum} -bootable devplist; env exists devplist || setenv devplist 1; for distro_bootpart in ${devplist}; do if fstype ${devtyp
scan_dev_for_efi=setenv efi_fdtfile ${fdtfile}; for prefix in ${efi_dtb_prefixes}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${efi_fdtfile}; then run lo
scan_dev_for_extlinux=if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${boot_syslinux_conf}; then echo Found ${prefix}${boot_syslinux_conf};                     
run boot_extlinux; echo SCRIPT FAILED: continuing...; fi^@                                                                                                                   
scan_dev_for_scripts=for script in ${boot_scripts}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${script}; then echo Found U-Boot script ${prefix}${script
scriptaddr=0x02400000^@                                                                                                                                                      
serial#=1000000082526380^@                                                                                                                                                   
soc=bcm283x^@                                                                                                                                                                
stderr=serial,vidconsole^@                                                                                                                                                   
stdin=serial,usbkbd^@                                                                                                                                                        
stdout=serial,vidconsole^@                                                                                                                                                   
usbethaddr=dc:a6:32:b8:81:f8^@                                                                                                                                               
vendor=raspberrypi^@                                                                                                                                                         
^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^
~                                                                                                                   

A quick comparison shows that what's in the file and in U-Boot env are the same

Does this mean that without loading the file we should also be able to boot?

Hmm or not really since we will need to combine the 2 files which are loaded

So let's try manually:

fdt addr ${fdt_addr} && fdt get value bootargs /chosen bootargs
fatload mmc 0:1 ${kernel_addr_r} Image
if test ! -e mmc 0:1 uboot.env; then saveenv; fi;
booti ${kernel_addr_r} - ${fdt_addr}

fdtfile

load ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r} ${prefix}${efi_fdtfile}
load mmc 0:1 ${fdt_addr_r} ${fdtfile}


--> 

U-Boot> fatload mmc 0:1 ${kernel_addr_r} Image
30157312 bytes read in 1930 ms (14.9 MiB/s)
U-Boot> echo $fdtfile
broadcom/bcm2711-rpi-4-b.dtb
U-Boot> load mmc 0:1 ${fdt_addr_r} bcm2711-rpi-4-b.dtb
47463 bytes read in 18 ms (2.5 MiB/s)
U-Boot> booti ${kernel_addr_r} - ${fdt_addr}

<--

-->
U-Boot> fatload mmc 0:1 ${kernel_addr_r} Image
30157312 bytes read in 1926 ms (14.9 MiB/s)
U-Boot> load mmc 0:1 ${fdt_addr_r} bcm2711-rpi-4-b.dtb
47463 bytes read in 18 ms (2.5 MiB/s)
U-Boot> booti ${kernel_addr_r} - ${fdt_addr}
<--

setenv autoload no
dhcp

U-Boot does not support networking!!!!
https://patchwork.ozlabs.org/project/uboot/cover/20200117012047.31096-1-andre.przywara@arm.com/

----

A recent version of U-Boot might work

----

setenv autoload no
dhcp

setenv serverip 192.168.42.108

setenv kernel Image
setenv bootfile ${hostname}/${kernel}

setenv kernel_netload tftp ${kernel_addr_r} ${bootfile}

setenv fdt bcm2711-rpi-4-b.dtb
setenv myfdtfile ${hostname}/${fdt}

setenv fdt_netload tftp ${fdt_addr_r} ${myfdtfile}

run kernel_netload

run fdt_netload

booti ${kernel_addr_r} - ${fdt_addr}

----


