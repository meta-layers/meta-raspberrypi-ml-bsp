LINUX_VERSION ?= "5.8.2"
LINUX_RPI_BRANCH ?= "rpi-5.8.y-2020-08-28"

SRCREV = "3affb6bda9e68c102901822504b02988695fe27d"

RPI_LINUX_KERNEL_REPO ?= "git://github.com/RobertBerger/raspberrypi-linux.git"

require linux-raspberrypi_5.8.inc

SRC_URI += " \
            file://powersave.cfg \
           "
