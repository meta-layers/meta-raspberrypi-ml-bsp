#### --> highly confusing
# those are version/branch/SRCREV for the upstream kernel
# NOT for the raspi kernel!
LINUX_VERSION ?= "5.8.5"
LINUX_RPI_BRANCH ?= "linux-5.8.y"
SRCREV = "9ece50d8a470ca7235ffd6ac0f9c5f0f201fe2c8"
#### <-- highly confusing

#### -->
#### -->
# those are the variables for the raspi kernel
# we need it as well since we copy over stuff 
# from there to the upstream kernel
RPI_LINUX_KERNEL_REPO ?= "git://github.com/RobertBerger/raspberrypi-linux.git"
RPI_LINUX_KERNEL_BRANCH ?= "rpi-5.8.y-2020-08-28"
RPI_LINUX_SRCREV = "3affb6bda9e68c102901822504b02988695fe27d"
#### <--

require linux-yocto-custom_5.8.inc

SRC_URI += " \
            file://powersave.cfg \
           "
