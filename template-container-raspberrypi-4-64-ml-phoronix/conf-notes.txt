
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:

    app-container-image-phoronix-oci

You can also run generated qemu images with a command like 'runqemu qemux86'
