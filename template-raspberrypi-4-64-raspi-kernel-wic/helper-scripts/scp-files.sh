#!/bin/bash

pushd /workdir/build/raspberrypi-4-64-raspi-kernel-wic/tmp/deploy/images/raspberrypi4-64

SOURCE_1="core-image-base-raspberrypi4-64*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/raspberrypi-4-64-raspi-kernel-wic/tmp/deploy/images/raspberrypi4-64"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
