#!/bin/bash

pushd /workdir/build/raspberrypi-4-64-ml-kernel-wic/tmp/deploy/images/raspberrypi4-64-ml

SOURCE_1="core-image-base-raspberrypi4-64-ml*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/raspberrypi-4-64-ml-kernel-wic/tmp/deploy/images/raspberrypi4-64-ml"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
