FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

DEPENDS += "rsync-native"

# added SERVERIP 
# different boot.cmd files
#   boot.cmd.in.sdcard  
#   boot.cmd.in.tftp

SRC_URI = "\
           file://boot.cmd.in.sdcard \
           file://boot.cmd.in.tftp \
          "

# we override the original do_compile task

do_compile() {

    # in local.conf: 
    #    KERNEL_FDT_FROM_SDCARD = "1"

    if [[ ${KERNEL_FDT_FROM_SDCARD} = *1* ]]
    then
        rsync -c ${WORKDIR}/boot.cmd.in.sdcard ${WORKDIR}/boot.cmd.in
    fi

    # in local.conf: 
    #    KERNEL_FDT_FROM_TFTP = "1"

    if [[ ${KERNEL_FDT_FROM_TFTP} = *1* ]]
    then
        rsync -c ${WORKDIR}/boot.cmd.in.tftp ${WORKDIR}/boot.cmd.in
    fi

    sed -e 's/@@KERNEL_IMAGETYPE@@/${KERNEL_IMAGETYPE}/' \
        -e 's/@@KERNEL_BOOTCMD@@/${KERNEL_BOOTCMD}/' \
        -e 's/@@SERVERIP@@/${SERVERIP}/' \
        "${WORKDIR}/boot.cmd.in" > "${WORKDIR}/boot.cmd"
    mkimage -A arm -T script -C none -n "Boot script" -d "${WORKDIR}/boot.cmd" boot.scr
}
