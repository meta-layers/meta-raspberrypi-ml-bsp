FILESEXTRAPATHS_prepend := "${THISDIR}/linux-raspberrypi:"

SRC_URI = " \
    git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;branch=${LINUX_RPI_BRANCH} \
    "
SRC_URI_remove = "file://rpi-kernel-misc.cfg"

require recipes-kernel/linux/linux-raspberrypi.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

KERNEL_EXTRA_ARGS_append_rpi = " DTC_FLAGS='-@ -H epapr'"

# --> subpaths from the raspi kernel
# raspi: arch/arm
RPI_LINUX_KERNEL_ARM_32_SUBPATH ?= "arch/arm"
SRC_URI += "${RPI_LINUX_KERNEL_REPO};branch=${RPI_LINUX_KERNEL_BRANCH};subpath=${RPI_LINUX_KERNEL_ARM_32_SUBPATH};name=archarm32"
SRCREV_archarm32 = "${RPI_LINUX_SRCREV}"

# raspi: arch/arm64
RPI_LINUX_KERNEL_ARM_64_SUBPATH ?= "arch/arm64"
SRC_URI += "${RPI_LINUX_KERNEL_REPO};branch=${RPI_LINUX_KERNEL_BRANCH};subpath=${RPI_LINUX_KERNEL_ARM_64_SUBPATH};name=archarm64"
SRCREV_archarm64 = "${RPI_LINUX_SRCREV}"

# raspi: drivers
RPI_LINUX_KERNEL_DRIVERS_SUBPATH ?= "drivers"
SRC_URI += "${RPI_LINUX_KERNEL_REPO};branch=${RPI_LINUX_KERNEL_BRANCH};subpath=${RPI_LINUX_KERNEL_DRIVERS_SUBPATH};name=drivers"
SRCREV_drivers = "${RPI_LINUX_SRCREV}"

# raspi: include
RPI_LINUX_KERNEL_INCLUDE_SUBPATH ?= "include"
SRC_URI += "${RPI_LINUX_KERNEL_REPO};branch=${RPI_LINUX_KERNEL_BRANCH};subpath=${RPI_LINUX_KERNEL_INCLUDE_SUBPATH};name=include"
SRCREV_include = "${RPI_LINUX_SRCREV}"
# <-- subpaths from the raspi kernel

# --> upstream kernel dtbo support
FILESEXTRAPATHS_prepend := "${THISDIR}/linux-yocto-custom/5.8.x/patches/:"

SRC_URI += "file://1000-dtbo-support.patch \
            file://1001-scripts-dtbo.patch \
           "
# <-- upstream kernel dtbo support

# --> eth/network support statically linked into kernel - for rootfs over nfs
FILESEXTRAPATHS_prepend := "${THISDIR}/linux-yocto-custom/5.8.x/fragments/:"

SRC_URI += "file://bcmgenet.cfg \
           "
# <-- eth/network support statically linked into kernel - for rootfs over nfs

# --> rsync is used afterwards
DEPENDS += "rsync-native"
# <-- rsync is used afterwards

do_configure_prepend() {
        # --> copy some "patches" from raspi linux to upstream linux
        #        modified:   arch/arm64/kernel/cpuinfo.c
        #        modified:   drivers/char/Kconfig
        #        modified:   drivers/char/Makefile
        #        modified:   drivers/clk/bcm/Kconfig
        #        modified:   drivers/clk/bcm/Makefile
        #        modified:   drivers/clk/bcm/clk-bcm2835.c
        #        modified:   drivers/firmware/raspberrypi.c
        #        modified:   drivers/of/Kconfig
        #        modified:   drivers/of/Makefile
        #        modified:   drivers/of/overlay.c
        #        modified:   include/soc/bcm2835/raspberrypi-firmware.h
        #
        #Untracked files:
        #  (use "git add <file>..." to include in what will be committed)
        #
        #        drivers/char/broadcom/
        #        drivers/clk/bcm/clk-bcm2711-dvp.c
        #        drivers/of/configfs.c
        #        include/linux/reset/reset-simple.h

        cp ${WORKDIR}/arm64/kernel/cpuinfo.c ${S}/arch/arm64/kernel/cpuinfo.c
        cp ${WORKDIR}/drivers/char/Kconfig ${S}/drivers/char/Kconfig
        cp ${WORKDIR}/drivers/char/Makefile ${S}/drivers/char/Makefile
        rsync -avp ${WORKDIR}/drivers/char/broadcom ${S}/drivers/char/
        cp ${WORKDIR}/drivers/clk/bcm/Kconfig ${S}/drivers/clk/bcm/Kconfig
        cp ${WORKDIR}/drivers/clk/bcm/Makefile ${S}/drivers/clk/bcm/Makefile
        cp ${WORKDIR}/drivers/clk/bcm/clk-bcm2835.c ${S}/drivers/clk/bcm/clk-bcm2835.c
        cp ${WORKDIR}/drivers/clk/bcm/clk-bcm2711-dvp.c ${S}/drivers/clk/bcm/clk-bcm2711-dvp.c
        cp ${WORKDIR}/drivers/firmware/raspberrypi.c ${S}/drivers/firmware/raspberrypi.c
        cp ${WORKDIR}/drivers/of/Kconfig ${S}/drivers/of/Kconfig
        cp ${WORKDIR}/drivers/of/Makefile ${S}/drivers/of/Makefile
        cp ${WORKDIR}/drivers/of/overlay.c ${S}/drivers/of/overlay.c
        cp ${WORKDIR}/drivers/of/configfs.c ${S}/drivers/of/configfs.c
        cp ${WORKDIR}/include/soc/bcm2835/raspberrypi-firmware.h ${S}/include/soc/bcm2835/raspberrypi-firmware.h
        cp ${WORKDIR}/include/linux/reset/reset-simple.h ${S}/include/linux/reset/reset-simple.h
	# <-- copy some "patches" from raspi linux to upstream linux

        # --> copy overlays from raspi linux to upstream linux
        rsync -avp ${WORKDIR}/arm/boot/dts/overlays ${S}/arch/arm/boot/dts/
        rsync -avp ${WORKDIR}/arm64/boot/dts/overlays ${S}/arch/arm64/boot/dts/
        # <-- copy overlays from raspi linux to upstream linux

        # --> copy device tree + dependencies from raspi linux to upstream linux
	# Changes not staged for commit:
	#  (use "git add <file>..." to update what will be committed)
	#  (use "git checkout -- <file>..." to discard changes in working directory)
	#
	#        modified:   arch/arm/boot/dts/bcm2711-rpi-4-b.dts
	#        modified:   arch/arm/boot/dts/bcm2711.dtsi
	#        modified:   arch/arm/boot/dts/bcm2835-rpi.dtsi
	#        modified:   arch/arm/boot/dts/bcm283x.dtsi
	#        modified:   arch/arm64/boot/dts/broadcom/bcm2711-rpi-4-b.dts
	#
	#Untracked files:
	#  (use "git add <file>..." to include in what will be committed)
	#
	#        arch/arm/boot/dts/bcm270x-rpi.dtsi
	#        arch/arm/boot/dts/bcm270x.dtsi
	#        arch/arm/boot/dts/bcm2711-rpi.dtsi
	#        arch/arm/boot/dts/bcm271x-rpi-bt.dtsi
	#        arch/arm/boot/dts/bcm283x-rpi-csi1-2lane.dtsi
	#        arch/arm/boot/dts/bcm283x-rpi-i2c0mux_0_44.dtsi
   
        cp ${WORKDIR}/arm/boot/dts/bcm2711-rpi-4-b.dts ${S}/arch/arm/boot/dts/bcm2711-rpi-4-b.dts
        cp ${WORKDIR}/arm/boot/dts/bcm2711.dtsi ${S}/arch/arm/boot/dts/bcm2711.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm2835-rpi.dtsi ${S}/arch/arm/boot/dts/bcm2835-rpi.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm283x.dtsi ${S}/arch/arm/boot/dts/bcm283x.dtsi
        cp ${WORKDIR}/arm64/boot/dts/broadcom/bcm2711-rpi-4-b.dts ${S}/arch/arm64/boot/dts/broadcom/bcm2711-rpi-4-b.dts
        cp ${WORKDIR}/arm/boot/dts/bcm270x-rpi.dtsi ${S}/arch/arm/boot/dts/bcm270x-rpi.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm270x.dtsi ${S}/arch/arm/boot/dts/bcm270x.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm2711-rpi.dtsi ${S}/arch/arm/boot/dts/bcm2711-rpi.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm271x-rpi-bt.dtsi ${S}/arch/arm/boot/dts/bcm271x-rpi-bt.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm283x-rpi-csi1-2lane.dtsi ${S}/arch/arm/boot/dts/bcm283x-rpi-csi1-2lane.dtsi
        cp ${WORKDIR}/arm/boot/dts/bcm283x-rpi-i2c0mux_0_44.dtsi ${S}/arch/arm/boot/dts/bcm283x-rpi-i2c0mux_0_44.dtsi
        # <-- copy device tree + dependencies from raspi linux to upstream linux
}
