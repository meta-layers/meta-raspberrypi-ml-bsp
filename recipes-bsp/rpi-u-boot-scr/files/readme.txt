for now - depending on what you want - copy either 
boot.cmd.in.sdcard to boot.cmd.in

or

boot.cmd.in.tftp to boot.cmd.in

you might need to force rebuilt rpi-u-boot-scr after a change:
rpi-u-boot-scr -c clean && bitbake rpi-u-boot-scr --no-setscene
