# if we have in local.conf
# ROOTFS_ON_NFS = "1"
# we need /var/lib/docker on a proper filesystem

do_install_append () {
    # in local.conf:
    #    ROOTFS_ON_NFS = "1"

    if [[ ${ROOTFS_ON_NFS} = *1* ]]
    then
    cat >> ${D}${sysconfdir}/fstab <<EOF

# with rootfs over nfs we need 
# a different file system for docker
/dev/mmcblk1p3  /var/lib/docker btrfs   defaults        0       0

EOF
   fi
}
