# Setup for arm-64-ml
COMPATIBLE_MACHINE = "^$"

COMPATIBLE_MACHINE_arm64-ml = "arm64-ml"
#COMPATIBLE_MACHINE_phyboard-polis-imx8mm = "phyboard-polis-imx8mm"
COMPATIBLE_MACHINE_raspberrypi4-64-ml = "raspberrypi4-64-ml"

# Default kernel config fragements for specific machines
#KERNEL_FEATURES_append_qemumicroblaze += "bsp/qemumicroblaze/qemumicroblaze.scc"
