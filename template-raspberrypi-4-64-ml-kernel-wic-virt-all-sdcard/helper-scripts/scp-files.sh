#!/bin/bash

pushd /workdir/build/raspberrypi-4-64-ml-kernel-wic-virt-all-sdcard/tmp/deploy/images/raspberrypi4-64-ml

SOURCE_1="core-image-minimal-virt-docker-ce-raspberrypi4-64-ml*.rootfs.wic.*"

TARGET_1="student@192.168.42.60:/home/student/projects/raspberrypi-4-64-ml-kernel-wic-virt-all-sdcard/tmp/deploy/images/raspberrypi4-64-ml"

echo "scp write-sd-card.sh ${TARGET_1}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
